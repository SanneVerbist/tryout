import { Card } from "antd";
import axios from "axios";
import React from "react";
import { FC, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Page } from "../components/page";

type RouteProps = {
  id: string;
};

export const PokemonDetail: FC = () => {
  const { id } = useParams<RouteProps>();
  const [pokemon, setPokemon] = React.useState({
    id: Number,
    name: "",
    type: [],
    base: {
      Attack: Number,
      Defense: Number,
      HP: Number,
      "Sp. Attack": Number,
      "Sp. Defense": Number,
      Speed: Number,
    },
  });

  useEffect(() => {
    axios.get(`http://localhost:4000/pokemons/${id}`).then((response) => {
      setPokemon(response.data);
    });
  }, [setPokemon, id]);

  return (
    <Page>
      <Card
        title={pokemon.name}
        size="default"
        style={{ width: 300, margin: 30 }}
      >
        <p>
          <strong>Id: </strong>
          {pokemon.id}
        </p>
        <p>
          <strong>Type(s): </strong>
        </p>
        {pokemon.type.map((item) => {
          return <p key={item}>{item}</p>;
        })}
        <p>
          <strong>Attack: </strong>
          {pokemon.base.Attack}
        </p>
        <p>
          <strong>Defence: </strong>
          {pokemon.base.Defense}
        </p>
        <p>
          <strong>HP: </strong>
          {pokemon.base.HP}
        </p>
        <p>
          <strong>Sp. Attack: </strong>
          {pokemon.base["Sp. Attack"]}
        </p>
        <p>
          <strong>Sp. Defence: </strong>
          {pokemon.base["Sp. Defense"]}
        </p>
        <p>
          <strong>Speed: </strong>
          {pokemon.base.Speed}
        </p>
      </Card>
    </Page>
  );
};
