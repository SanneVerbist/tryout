import React, { FC, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Page } from "../components/page";
import axios from "axios";
import { Table, Tag } from "antd";

export const PokemonList: FC = () => {
  const history = useHistory();
  const [dataSource, setData] = React.useState([]);

  useEffect(() => {
    axios.get("http://localhost:4000/pokemons").then((response) => {
      setData(response.data);
    });
  }, [setData]);

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    { title: "Name", dataIndex: "name", key: "name" },
    {
      title: "Type",
      dataIndex: "type",
      key: "type",
      render: (type: Array<string>) => (
        <>
          {type.map((type) => {
            return <Tag key={type}>{type}</Tag>;
          })}
        </>
      ),
    },
  ];

  const selectRow = (record: any) => {
    history.push(`/${record.id}`);
  };

  return (
    <Page>
      <Table
        dataSource={dataSource}
        columns={columns}
        bordered
        onRow={(record) => ({
          onClick: () => {
            selectRow(record);
          },
        })}
        style={{ margin: 30 }}
      />
      ;
    </Page>
  );
};
