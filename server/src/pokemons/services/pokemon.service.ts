import { Injectable } from '@nestjs/common';
import * as pokemonJson from '../data/pokemons.json';
import { PokemonDto } from '../dto/pokemon.dto';

@Injectable()
export class PokemonService {
    private pokemons: PokemonDto[];

    constructor() {
        this.pokemons = pokemonJson;
    }

    findAll(): PokemonDto[] {
        return this.pokemons;
    }

    findOne(id: string): PokemonDto {
        let pokemonList = this.findAll();
        return pokemonList.find((pokemon) => pokemon.id === Number(id));
    }
}
